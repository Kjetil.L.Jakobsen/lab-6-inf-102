package INF102.lab6.cheapFlights;

import java.util.ArrayList;
import java.util.List;

public class Test {
    static ICheapestFlight cheapestFlight = new CheapestFlight();
    public static void main(String[] args) {
        List<Flight> flights = flights4();
        City start = City.OSLO;
        City destination = City.BODØ;
        int maxNumberStops = 2;

        cheapestFlight.findCheapestFlights(flights, start, destination, maxNumberStops);
    }

    private static List<Flight> flights4() {
        List<Flight> flights = new ArrayList<>();
        flights.add(new Flight(City.OSLO, City.TRONDHEIM, 400));
        flights.add(new Flight(City.TRONDHEIM, City.BODØ, 500));
        flights.add( new Flight(City.OSLO, City.BERGEN, 100));
        flights.add(new Flight(City.BERGEN, City.TRONDHEIM, 100));
        flights.add(new Flight(City.TRONDHEIM, City.TROMSØ, 100));
        flights.add(new Flight(City.TROMSØ, City.BODØ, 100));

        return flights;
    }
    
}
