package INF102.lab6.cheapFlights;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.Collections;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();
        for (Flight f : flights) {
            graph.addVertex(f.start);
            graph.addVertex(f.destination);
            graph.addEdge(new DirectedEdge<City>(f.start, f.destination), f.cost);
        }
        return graph;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);

        //DFS recursive stratergy
        return findCheapShortPath(graph, start, destination, nMaxStops, 0);



        /* 

        //DFS non-recursive stratergy
        HashSet<City> found1 = new HashSet<>();
        Stack<City> toSearch1 = new Stack<>();
        toSearch1.add(start);
        ArrayList<City> path = new ArrayList<>();
        while (!toSearch1.isEmpty()) {
            City c = toSearch1.pop();
            path.add(c);
            //Copy path to paths, do not do below if statement
            if (!found1.contains(c)) {
                found1.add(c);
                for (City nc : graph.outNeighbours(c)) {
                    toSearch1.add(nc);
                }
            }
        }



        //BFS stratergy
        //ArrayList<City> flightPath = new ArrayList<>();
        //HashMap<City, ArrayList<City>> found = new HashMap<>();
        //Map from destination to a list containing amount of stops and cost
        HashMap<City, ArrayList<Integer>> found = new HashMap<>();

        //HashMap<City, Integer> found = new HashMap<>();
        //HashSet<City> set = new HashSet<>();
        PriorityQueue<City> toSearch = new PriorityQueue<>();
        //Stack<City> toSearch = new Stack<>(); 
        found.put(start, new ArrayList<>(Arrays.asList(0, 0)));
        toSearch.add(start);
        while(!toSearch.isEmpty()) {
            City prevCity = toSearch.poll();

            System.out.println("prevCity = " + prevCity);
            System.out.println("toSearch = " + toSearch);
            System.out.println("found = " + found + "\n");

            if (found.get(prevCity).get(0) <= nMaxStops) {   // < or <=  ?
                for (City nextCity : graph.outNeighbours(prevCity)) {
                    toSearch.add(nextCity);
                    if (found.containsKey(nextCity)) {
                        if (found.get(prevCity).get(1) + graph.getWeight(prevCity, nextCity) < found.get(nextCity).get(1)) {
                            found.get(nextCity).set(0, found.get(prevCity).get(0) + 1);
                            found.get(nextCity).set(1, found.get(prevCity).get(1) + graph.getWeight(prevCity, nextCity));
                        }
                    }
                    else {
                        found.put(nextCity, new ArrayList<>(Arrays.asList(
                                  found.get(prevCity).get(0) + 1,
                                  found.get(prevCity).get(1) + graph.getWeight(prevCity, nextCity)
                        )));         
                    }
                }
            }
        }
        return found.get(destination).get(1);
        
        */
    }

    private int findCheapShortPath(WeightedDirectedGraph<City, Integer> graph, City current, City destination,
            int remainingStops, int value) {

        if (current.equals(destination)) {
            return value;
        }
        if (remainingStops == -1) {
            return Integer.MAX_VALUE;   //Possible bit overflow?
        }
        ArrayList<Integer> list = new ArrayList<>();
        for (City c : graph.outNeighbours(current)) {
            list.add(findCheapShortPath(graph, c, destination, remainingStops-1, graph.getWeight(current, c) + value));
        }
        return Collections.min(list);
    }
    
}
